import Transport from '@ledgerhq/hw-transport'
import { Path } from './utils/Path';

export default class Koinos {
    transport: Transport;

    readonly CLA = 0xE0;
    readonly INS_GET_PUBLIC_KEY = 0x05;

    constructor(transport: Transport, scrambleKey: string = 'Koinos') {
        this.transport = transport
        transport.decorateAppAPIMethods(
            this,
            ["getAddress"],
            scrambleKey
        )
    }

    async getAddress(path: string, confirm?: boolean): Promise<{ publicKey: string, address: string, chainCode: string }> {
        const bipPath = new Path(path)
        const result = await this.transport.send(this.CLA, this.INS_GET_PUBLIC_KEY, confirm ? 0x01 : 0x00, 0x00, bipPath.encode());

        let offset = 0, len;

        len = result[offset];
        const address = result.slice(offset + 1, offset + 1 + len).toString('ascii');
        offset += len + 1;

        len = result[offset];
        const publicKey = result.slice(offset + 1, offset + 1 + len).toString('hex');
        offset += len + 1;

        len = result[offset];
        const chainCode = result.slice(offset + 1, offset + 1 + len).toString('hex');

        return {
            address,
            publicKey,
            chainCode
        }
    }

}
