const BIPPath = require('bip32-path');

export class Path {

    bipArray: any[];

    constructor(path: string) {
        this.bipArray = BIPPath.fromString(path).toPathArray();
    }

    public encode(): Buffer {
        const encodedPath = Buffer.alloc(1 + this.bipArray.length * 4);

        encodedPath.writeInt8(this.bipArray.length);

        this.bipArray.forEach((segment: number, index: number) => {
            encodedPath.writeUInt32BE(segment, 1 + (index * 4));
        });

        return encodedPath;
    }

    get size(): number {
        return this.bipArray.length;
    }

}
