# @engrave/hw-app-koinos

Ledger Hardware Wallet HIVE JavaScript/TypeScript bindings.

# Example usage

```typescript
import Koinos from '@engrave/hw-app-koinos';
import TransportNodeHid from '@ledgerhq/hw-transport-node-hid';

const transport = await TransportNodeHid.create();
const hive = new Koinos(transport);

const key = await koinos.getAddress(`m/44'/60'/0'/0/0`, true);               // eth compatible path
console.log("Received key:", key);
```

For more examples, visit `examples` directory.

## API

* [getAddress](#getaddress)

## getAddress

Retrieve public key and address from specified BIP32 path. You can set additional parameter `confirm: boolean` which will ask the user to confirm the Public Key on his Ledger before returning it.

```typescript
async getAddress(path: string, confirm?: boolean): Promise<{ publicKey: string, address: string, chainCode: string }> {
```
