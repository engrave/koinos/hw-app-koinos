import Koinos from '../src/Koinos';
import SpeculosTransport from '@ledgerhq/hw-transport-node-speculos';

// https://iancoleman.io/bip39/
// twice sad core job rent mutual knife armor label diamond push faint
// path                 address                             public key (compressed)
// m/44'/60'/0'/0/0     1Ds4TF4KRrHaGzfjWgbKDoCz4zBqFGxGkE  0360c5792cc3bd8a458255b7a714fc8709bfc554fdaaf0ea1562404f0effaaa64e

( async () => {

    console.log('Unlock your ledger and open Koinos app....');
    const transport = await SpeculosTransport.open({apduPort: 40000});
    console.log(`Established transport with Ledger Nano`);

    try {
        const koinos = new Koinos(transport);
        // const key = await koinos.getAddress(`m/44'/659'/0'/0/0`, true);     // koinos path
        const key = await koinos.getAddress(`m/44'/60'/0'/0/0`, true);               // eth compatible path
        console.log("Received key:", key);
    } catch (e) {
        console.error(e);
    }
    finally {
        await transport.close();
    }

})();
